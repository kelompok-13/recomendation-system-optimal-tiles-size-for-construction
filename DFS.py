from collections import defaultdict

class Graph:

    def __init__(self):

        self.graph = defaultdict(list)

    def addEdge(self, u, v):
        self.graph[u].append(v)

    def DFSUtil(self, v, visited):

        visited[v] = True
        print(v, " ")

        for i in self.graph[v]:

            if visited[i] == False:

                self.DFSUtil(i, visited)


    def DFS(self, v):

        visited = [False] * (len(self.graph))

        self.DFSUtil(v, visited)

g = Graph()

g.addEdge(1 , 1)
g.addEdge(1 , 2)
g.addEdge(2 , 1)
g.addEdge(2 , 2)
g.addEdge(2 , 3)
g.addEdge(3 , 2)
g.addEdge(3 , 5)
g.addEdge(4 , 2)
g.addEdge(4 , 3)
g.addEdge(4 , 4)
g.addEdge(4 , 20)
g.addEdge(5 , 5)
g.addEdge(5 , 6)
g.addEdge(5 , 10)
g.addEdge(5 , 20)
g.addEdge(5 , 22)
g.addEdge(5 , 67)
g.addEdge(5 , 22)
g.addEdge(6 , 4)
g.addEdge(6 , 6)
g.addEdge(6 , 9)
g.addEdge(6 , 7)
g.addEdge(6 , 11)
g.addEdge(6 , 12)
g.addEdge(7 , 7)
g.addEdge(7 , 8)
g.addEdge(7 , 14)
g.addEdge(7 , 30)
g.addEdge(8 , 4)
g.addEdge(8 , 9)
g.addEdge(8 , 15)
g.addEdge(8 , 30)
g.addEdge(9 , 10)
g.addEdge(9 , 12)
g.addEdge(9 , 18)
g.addEdge(9 , 24)
g.addEdge(10 , 10)
g.addEdge(10 , 20)
g.addEdge(10 , 30)
g.addEdge(10 , 60)
g.addEdge(10 , 11)
g.addEdge(11 , 11)
g.addEdge(11 , 14)
g.addEdge(14 , 14)
g.addEdge(15 , 2)
g.addEdge(15 , 30)
g.addEdge(15 , 90)
g.addEdge(17 , 67)
g.addEdge(18 , 18)
g.addEdge(20 , 20)
g.addEdge(20 , 25)
g.addEdge(20 , 30)
g.addEdge(20 , 120)
g.addEdge(20 , 60)
g.addEdge(23 , 90)
g.addEdge(23 , 45)
g.addEdge(25 , 33)
g.addEdge(25 , 40)
g.addEdge(25 , 25)
g.addEdge(25 , 33)
g.addEdge(25 , 38)
g.addEdge(25 , 50)
g.addEdge(25 , 75)
g.addEdge(30 , 30)
g.addEdge(30 , 45)
g.addEdge(30 , 90)
g.addEdge(30 , 60)
g.addEdge(30 , 61)
g.addEdge(30 , 120)
g.addEdge(33 , 33)
g.addEdge(33 , 67)
g.addEdge(33 , 67)
g.addEdge(33 , 33)
g.addEdge(34 , 39)
g.addEdge(35 , 98)
g.addEdge(40 , 40)
g.addEdge(40 , 80)
g.addEdge(41 , 41)
g.addEdge(45 , 45)
g.addEdge(45 , 90)
g.addEdge(60 , 60)
g.addEdge(60 , 120)
g.addEdge(61 , 122)
g.addEdge(75 , 75)
g.addEdge(76 , 114)
g.addEdge(81 , 3)
g.addEdge(80 , 80)
g.addEdge(80 , 120)
g.addEdge(98 , 200)
g.addEdge(100 , 100)
g.addEdge(120, 120)


g.DFS(8)